# Write a class that meets these requirements.
#
# Name:       Person
#
# Required state:
#    * name, a string
#    * hated foods list, a list of names of food they don't like
#    * loved foods list, a list of names of food they really do like
#
# Behavior:
#    * taste(food name)  * returns None if the food name is not in their
#                                  hated or loved food lists
#                        * returns True if the food name is in their
#                                  loved food list
#                        * returns False if the food name is in their
#                                  hated food list
#
# Example:
#    person = Person("Malik",
#                    ["cottage cheese", "sauerkraut"],
#                    ["pizza", "schnitzel"])
#
#    print(person.taste("lasagna"))     # Prints None, not in either list
#    print(person.taste("sauerkraut"))  # Prints False, in the hated list
#    print(person.taste("pizza"))       # Prints True, in the loved list


# class Person
# method initializer with name, hated foods list, and loved foods list
# self.name = name
# self.hated_foods = hated_foods
# self.loved_foods = loved_foods
# method taste(self, food)
# if food is in self.hated_foods
# return False
# otherwise, if food is in self.loved_foods
# return True
# otherwise
# return None

class Person:
    def __init__(self, name, hated_food_list, loved_food_list):
        self.name = name
        self.hated_food_list = hated_food_list
        self.loved_food_list = loved_food_list

    def taste(self, food_name):
        # if (
        #     food_name not in self.hated_food_list or
        #     food_name not in self.loved_food_list
        # ):
        #     return None
        if food_name in self.loved_food_list:
            return True
        elif food_name in self.hated_food_list:
            return False


person = Person("Wagner", ["Sweet Potatos", "Paella"], ["Pasta", "Salmon"])
print(person.taste("Green Beans"))
print(person.taste("Salmon"))
print(person.taste("Sweet Potatos"))
