# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special passwordacter $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    special = ["$", "!", "@"]
    if not (len(password) >= 6 and len(password) <= 12):
        return "Password length requirements not met."

    if (
            any(char.islower() for char in password)
            and any(char.isupper() for char in password)
            and any(char.isdigit()for char in password)
            and any(char.isalpha() for char in password)
            and any(char in special for char in password)
    ):
        return "Password valid."
    return "Password requirements not met."


print(check_password("A0z@ac"))
print(check_password("A0Z$AC"))
print(check_password("A0z!ac"))
print(check_password("A0z@"))
print(check_password("A0zadc"))
