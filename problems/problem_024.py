# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
import math


def calculate_average(values):
    sum_values = sum(values)
    average = sum_values / len(values)
    if len(values) == 0:
        return None
    return math.floor(average)


print(calculate_average([2, 4, 5, 6, 6, 6, 6]))
