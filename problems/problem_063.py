# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    mylist = []
    second_l = []
    for i in string:
        if i == "Z" or i == "z":
            mylist.append(ord(i) - 25)
        else:
            mylist.append(ord(i) + 1)
    for j in mylist:
        second_l.append(chr(j))
    return "".join(second_l)


print(shift_letters("import"))
print(shift_letters("ABBA"))
print(shift_letters("KalZ"))
print(shift_letters("zap"))
