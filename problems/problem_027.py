# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    max_value = 0
    if len(values) == 0:
        return None
    for num in values:
        if num > max_value:
            max_value = num
    return max_value


print(max_in_list([21, 6, 7, 12]))
print(max_in_list([1, 3, 2, 12]))
print(max_in_list([]))
