# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one
import random


def specific_random():
    mylist = []
    while len(mylist) < 10:
        r_num = random.randint(10, 500)
        if r_num % 5 == 0 and r_num % 7 == 0:
            mylist.append(r_num)
    return random.choice(mylist)


print(specific_random())
print(specific_random())
print(specific_random())
