# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    values_ = dictionary.values()
    keys = dictionary.keys()
    new_dictionary = dict(zip(values_, keys))
    return new_dictionary


print(reverse_dictionary({}))
print(reverse_dictionary({"key": "value"}))
print(reverse_dictionary({"key1": 1, "key2": 2, "key3": 100}))
