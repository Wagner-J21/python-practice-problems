# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    # I am going to make a list containing the neccesary ingrediants
    pasta_ingrediants = ["flour", "eggs", "oil"]
    # I am gong to loop over my list
    if all(item in pasta_ingrediants for item in ingredients):
        return True
    else:
        return False


print(can_make_pasta(["salt", "pepper", "cheese"]))
print(can_make_pasta(["flour", "eggs", "oil"]))
