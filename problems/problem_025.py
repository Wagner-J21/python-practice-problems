# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    sum_values = sum(values)
    if sum_values == 0:
        return None
    return sum_values


print(calculate_sum([2, 4, 5, 6, 6, 6, 6]))
