# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    sorted_numbers = sorted(values)
    # sec_val = len(sorted)
    second_Lg = sorted_numbers[-2]

    if len(values) == 1 or len(values) == 0:
        return second_Lg
    return second_Lg


print(find_second_largest([21, 2, 12, 10]))
