# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    x_inbound = False
    y_inbound = False
    if x >= 0 and x <= 10:
        x_inbound = True

    if y >= 0 and y <= 10:
        y_inbound = True
    return x_inbound, y_inbound


print(is_inside_bounds(9, 10))
