# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.

def pairwise_add(list1, list2):
    sum_list = []
    zipper = zip(list1, list2)
    for num in zipper:
        sum_list.append(sum(num))
    return sum_list


print(pairwise_add([], []))
print(pairwise_add([3], [8]))
print(pairwise_add([3, 1, 1000], [8, 1010, 9]))
