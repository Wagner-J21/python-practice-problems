# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    num_members = len(members_list)
    half_members = num_members / 2
    print(num_members)
    if len(attendees_list) >= half_members:
        return "Too many Peaple!!"
    return "We can accept more people"


print(has_quorum(
    ["billy", "ray", "cyrus", "luis", "felix", "shullissa]"],
    ["Gronky", "Mia", "Jay"])
)
print(has_quorum(
    ["steph", "angie"], ["Gronky", "Mia", "jay", "Emma", "Luffy", "Nami"])
)
