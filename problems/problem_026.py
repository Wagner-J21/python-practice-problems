# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average
import math


def calculate_grade(values):
    grade_average = math.floor(sum(values) / len(values))
    # print(grade_average)
    grade = ""
    if grade_average >= 90:
        grade = "A"
    elif grade_average >= 80 and grade_average < 90:
        grade = "B"
    elif grade_average < 80 and grade_average >= 70:
        grade = "C"
    elif grade_average < 70 and grade_average >= 60:
        grade = "D"
    else:
        grade = "F"
    return grade


print(calculate_grade([95, 90, 92]))
print(calculate_grade([85, 80, 82]))
print(calculate_grade([75, 70, 72]))
print(calculate_grade([65, 60, 62]))
print(calculate_grade([55, 50, 52]))
